﻿namespace AutoVerzekeringsPremieTest
{
    using System;
    using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
    using Xunit;

    public class VehicleTest
    {
        [Fact]
        public void VehicleCalculateAgeTest()
        {
            //Arrange
            var age = 5;
            var constructionYear = DateTime.Now.Year - age;

            //Act
            var vehicle = new Vehicle(1, 1, constructionYear);

            //Assert
            Assert.Equal(age, vehicle.Age);
        }

        [Fact]
        public void VehicleCalculateConstructCurrentYearTest()
        {
            //Arrange
            var constructionYear = DateTime.Now.Year;

            //Act
            var vehicle = new Vehicle(1, 1, constructionYear);

            //Assert
            Assert.Equal(0, vehicle.Age);
        }

        [Fact]
        public void VehicleCalculateConstructInFutureTest()
        {
            //Arrange
            var constructionYear = DateTime.Now.Year + 1;

            //Act
            var vehicle = new Vehicle(1, 1, constructionYear);

            //Assert
            Assert.Equal(0, vehicle.Age);
        }
    }
}