﻿namespace AutoVerzekeringsPremieTest
{
    using System;
    using System.Reflection;
    using Moq;
    using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
    using Xunit;

    public class PremiumCalculationTest
    {
        [Theory]
        [InlineData(90,15000,10)]
        [InlineData(0,0,10)]
        public void CalculateBasePremiumTest(int powerInKw, int priceInEuro, int age)
        {
            //Arrange
            var vehicle = new Mock<IVehicle>();
            vehicle.Setup(v => v.PowerInKw).Returns(powerInKw);
            vehicle.Setup(v => v.ValueInEuros).Returns(priceInEuro);
            vehicle.Setup(v => v.Age).Returns(age);
            //var vehicle = new Vehicle(powerInKw, priceInEuro, DateTime.Now.Year - age);

            //Expected Waarde (voertuig / 100 - leeftijd + vermogen in KW / 5) / 3
            var expectedValue =  ((double)priceInEuro / 100 - age + (double)powerInKw / 5) / 3;
            expectedValue = expectedValue < 0 ? 0 : expectedValue;

            //Act
            var basePremium = PremiumCalculation.CalculateBasePremium(vehicle.Object);

            //Assert
            Assert.Equal(expectedValue, basePremium);
        }

        [Theory]
        [InlineData(530.76)]
        [InlineData(100/0.975)]
        [InlineData(double.MaxValue)]
        [InlineData(double.MinValue)]
        [InlineData(0)]
        public void PremiumPaymentAmountDiscountForYearPaymentTest(double yearPremium)
        {
            //Arrange
            var vehicle = new Vehicle(1, 1, 1);
            var policyHolder = new PolicyHolder(1, "1-1-2020", 1, 1);
            var premiumCalculator = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            var premiumProperty = typeof(PremiumCalculation).GetProperty("PremiumAmountPerYear");
            if (premiumProperty is null)
                Assert.False(true, "Can not catch the property");

            premiumProperty.SetValue(premiumCalculator, yearPremium);

            var paymentMethod = typeof(PremiumCalculation).GetMethod("PremiumPaymentAmount",
                BindingFlags.NonPublic | BindingFlags.Instance);
            if (paymentMethod is null)
                Assert.False(true, "Can not catch the payment method");

            //Act
            var actualValue = paymentMethod.Invoke(premiumCalculator, new object[] { PremiumCalculation.PaymentPeriod.YEAR });

            //Assert
            Assert.Equal(Math.Round(yearPremium*0.975, 2), actualValue);
        }

        [Theory]
        [InlineData(530.76)]
        [InlineData(120)]
        [InlineData(double.MaxValue)]
        [InlineData(double.MinValue)]
        [InlineData(0)]
        public void PremiumPaymentAmountPerMonthTest(double yearPremium)
        {
            //Arrange
            var vehicle = new Vehicle(1, 1, 1);
            var policyHolder = new PolicyHolder(1, "1-1-2020", 1, 1);
            var premiumCalculator = new PremiumCalculation(vehicle, policyHolder, InsuranceCoverage.WA);

            var premiumProperty = typeof(PremiumCalculation).GetProperty("PremiumAmountPerYear");
            if (premiumProperty is null)
                Assert.False(true, "Can not catch the property");

            premiumProperty.SetValue(premiumCalculator, yearPremium);

            var paymentMethod = typeof(PremiumCalculation).GetMethod("PremiumPaymentAmount",
                BindingFlags.NonPublic | BindingFlags.Instance);
            if (paymentMethod is null)
                Assert.False(true, "Can not catch the payment method");

            //Act
            var actualValue = paymentMethod.Invoke(premiumCalculator, new object[] { PremiumCalculation.PaymentPeriod.MONTH });

            //Assert
            Assert.Equal(Math.Round(yearPremium/12, 2), actualValue);
        }

        [Theory]
        [InlineData(100, int.MinValue)]
        [InlineData(100, 0)]
        [InlineData(100, 5)]
        [InlineData(100, 6)]
        [InlineData(100, 17)]
        [InlineData(100, 18)]
        [InlineData(100, 19)]
        [InlineData(100, int.MaxValue)]
        public void NoClaimDiscountTest(double premium, int years)
        {
            //Arrange
            var noClaimDiscountMethod = typeof(PremiumCalculation)
                .GetMethod("UpdatePremiumForNoClaimYears",
                    BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic);

            if (noClaimDiscountMethod is null)
                Assert.False(true, "Can not catch the method");

            double discountPercentage = years > 5 ? (years<18?(years - 5) * 5:65): 0;

            var expectedValue = premium * (1 - discountPercentage / 100);

            //Act
            var actual = noClaimDiscountMethod.Invoke(null, new object[] {premium, years});

            //Assert
            Assert.Equal(expectedValue, actual);
        }

        [Theory]
        [InlineData(100, int.MinValue, 100)]
        [InlineData(100, 999, 100)]
        [InlineData(100, 1000, 100 * 1.05)]
        [InlineData(100, 3599, 100 * 1.05)]
        [InlineData(100, 3600, 100 * 1.02)]
        [InlineData(100, 4499, 100 * 1.02)]
        [InlineData(100, 4500, 100)]
        [InlineData(100, int.MaxValue, 100)]
        public void UpdatePremiumForPostalCodeTest(double premium, int postalCode, double expectedValue)
        {
            //Arrange
            var postalCodeUpdateMethod = typeof(PremiumCalculation)
                .GetMethod("UpdatePremiumForPostalCode",
                    BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic);
            //Act
            if (postalCodeUpdateMethod is null)
                Assert.False(true, "Can not catch the method");

            //Act
            var actualValue = postalCodeUpdateMethod.Invoke(null, new object[] {premium, postalCode});

            //Assert
            Assert.Equal(expectedValue, actualValue);
        }

        [Fact]
        public void PremiumTestForYoungerThanTwentyThreeYears()
        {
            //Arrange
            //var vehicle = new Vehicle(90, 15000, 2010);
            var vehicle = new Mock<IVehicle>();
            vehicle.Setup(x => x.PowerInKw).Returns(90);
            vehicle.Setup(x => x.ValueInEuros).Returns(15000);
            vehicle.Setup(x => x.Age).Returns(10);

            var policyHolder = new Mock<IPolicyHolder>();
            policyHolder.SetupSequence(p => p.Age).Returns(22).Returns(23);
            policyHolder.Setup(p => p.LicenseAge).Returns(6);
            policyHolder.Setup(p => p.PostalCode).Returns(9999);
            policyHolder.Setup(p => p.NoClaimYears).Returns(0);


            //var policyHolderYong = new PolicyHolder(22, DateTime.Now.AddYears(-6).ToString(), 9999, 0);
            //var policyHolderOld = new PolicyHolder(23, DateTime.Now.AddYears(-6).ToString(), 9999, 0);

            //Act
            var premiumYong = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA);
            var premiumOld = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premiumOld.PremiumAmountPerYear * 1.15, premiumYong.PremiumAmountPerYear);
        }


        [Fact]
        public void PremiumTestForDriverWithLessThanSixYearsExperience()
        {
            //Arrange
            //var vehicle = new Vehicle(90, 15000, 2010);
            var vehicle = new Mock<IVehicle>();
            vehicle.Setup(x => x.PowerInKw).Returns(90);
            vehicle.Setup(x => x.ValueInEuros).Returns(15000);
            vehicle.Setup(x => x.Age).Returns(10);

            var policyHolder = new Mock<IPolicyHolder>();
            policyHolder.Setup(p => p.Age).Returns(25);
            policyHolder.SetupSequence(p => p.LicenseAge).Returns(5).Returns(6);
            policyHolder.Setup(p => p.PostalCode).Returns(9999);
            policyHolder.Setup(p => p.NoClaimYears).Returns(0);

            //var policyHolderYongDriver = new PolicyHolder(25, DateTime.Now.AddYears(-5).ToString(), 9999, 0);
            //var policyHolderOldDriver = new PolicyHolder(25, DateTime.Now.AddYears(-6).ToString(), 9999, 0);

            //Act
            var premiumYong = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA);
            var premiumOld = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premiumOld.PremiumAmountPerYear * 1.15, premiumYong.PremiumAmountPerYear);
        }

        [Fact]
        public void PremiumTestCoverageIncrementWA_Plus()
        {
            //Arrange
            //var vehicle = new Vehicle(90, 15000, 2010);
            var vehicle = new Mock<IVehicle>();
            vehicle.Setup(x => x.PowerInKw).Returns(90);
            vehicle.Setup(x => x.ValueInEuros).Returns(15000);
            vehicle.Setup(x => x.Age).Returns(10);

            //var policyHolder = new PolicyHolder(25, DateTime.Now.AddYears(-6).ToString(), 9999, 0);
            var policyHolder = new Mock<IPolicyHolder>();
            policyHolder.Setup(p => p.Age).Returns(25);
            policyHolder.Setup(p => p.LicenseAge).Returns(6);
            policyHolder.Setup(p => p.PostalCode).Returns(9999);
            policyHolder.Setup(p => p.NoClaimYears).Returns(0);

            //Act
            var premiumWA_Plus = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA_PLUS);
            var premiumWA = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premiumWA.PremiumAmountPerYear * 1.2, premiumWA_Plus.PremiumAmountPerYear);
        }

        [Fact]
        public void PremiumTestCoverageIncrementAllRisk()
        {
            //Arrange
            //var vehicle = new Vehicle(90, 15000, 2010);
            var vehicle = new Mock<IVehicle>();
            vehicle.Setup(x => x.PowerInKw).Returns(90);
            vehicle.Setup(x => x.ValueInEuros).Returns(15000);
            vehicle.Setup(x => x.Age).Returns(10);

            //var policyHolder = new PolicyHolder(25, DateTime.Now.AddYears(-6).ToString(), 9999, 0);
            var policyHolder = new Mock<IPolicyHolder>();
            policyHolder.Setup(p => p.Age).Returns(25);
            policyHolder.Setup(p => p.LicenseAge).Returns(6);
            policyHolder.Setup(p => p.PostalCode).Returns(9999);
            policyHolder.Setup(p => p.NoClaimYears).Returns(0);

            //Act
            var premiumAllRisk = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.ALL_RISK);
            var premiumWA = new PremiumCalculation(vehicle.Object, policyHolder.Object, InsuranceCoverage.WA);

            //Assert
            Assert.Equal(premiumWA.PremiumAmountPerYear * 2, premiumAllRisk.PremiumAmountPerYear);
        }
    }
}