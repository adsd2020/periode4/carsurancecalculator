﻿namespace AutoVerzekeringsPremieTest
{
    using System;
    using System.Globalization;
    using System.Reflection;
    using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
    using Xunit;

    public class PolicyHolderTest
    {
        [Fact]
        public void TestParseDateReturnADateTimeObject()
        {
            var parseDate = typeof(PolicyHolder).GetMethod("ParseDate", BindingFlags.NonPublic | BindingFlags.Static);
            var result = (DateTime) parseDate.Invoke(new DateTime(), new[] {"25-1-2020"});

            Assert.Equal(result, new DateTime(2020, 1, 25));
        }

        [Fact]
        public void TestParseDateReturnException()
        {
            var parseDate = typeof(PolicyHolder).GetMethod("ParseDate", BindingFlags.NonPublic | BindingFlags.Static);

            Assert.Throws<TargetInvocationException>(() => parseDate.Invoke(new DateTime(), new[] {""}));
        }

        [Fact]
        public void TestAgeByDateForDayAndMonthAfterToday()
        {
            //Arrange
            var ageByDateMethod = typeof(PolicyHolder).GetMethod("AgeByDate",
                BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic);
            //Act
            var age = ageByDateMethod.Invoke(null, new object[] { DateTime.Today.AddYears(-2).AddMonths(-2) });

            //Assert
            Assert.Equal(2,age);
        }

        [Fact]
        public void TestAgeByDateForDayAndMonthBeforToday()
        {
            //Arrange
            var ageByDateMethod = typeof(PolicyHolder).GetMethod("AgeByDate",
                BindingFlags.Static | BindingFlags.Instance | BindingFlags.NonPublic);

            //Act
            var age = ageByDateMethod.Invoke(null, new object[] { DateTime.Today.AddYears(-2).AddMonths(2) });

            //Assert
            Assert.Equal(1, age);
        }
    }
}