﻿using System;
using WindesheimAD2021AutoVerzekeringsPremie.Implementation;
using static WindesheimAD2021AutoVerzekeringsPremie.Implementation.PremiumCalculation;

namespace WindesheimAD2021AutoVerzekeringsPremie
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine(@"  _____          _____   _____ _    _ _____  ______ ");
            Console.WriteLine(@" / ____|   /\   |  __ \ / ____| |  | |  __ \|  ____|");
            Console.WriteLine(@"| |       /  \  | |__) | (___ | |  | | |__) | |__   ");
            Console.WriteLine(@"| |      / /\ \ |  _  / \___ \| |  | |  _  /|  __|  ");
            Console.WriteLine(@"| |____ / ____ \| | \ \ ____) | |__| | | \ \| |____ ");
            Console.WriteLine(@" \_____/_/    \_\_|  \_\_____/ \____/|_|  \_\______|");
            Console.WriteLine(@"~~~ Welcome to the CarSure insurance calculator! ~~~" + "\r\n\r\n");
            Console.ResetColor();
            var startInputLine = Console.CursorTop;

            PolicyHolder policyholder = GetPolicyHolderData();
            ResetConsoleState(startInputLine);

            Vehicle vehicle = GetVehicleData();
            ResetConsoleState(startInputLine);

            InsuranceCoverage coverage = GetCoverage();
            ResetConsoleState(startInputLine);


            DisplayPremium(policyholder, vehicle, coverage);
        }

        
        private static PolicyHolder GetPolicyHolderData()
        {
            Console.WriteLine("Please provide the policy holder's details...\r\n");
            Console.WriteLine("What is the policy holder's age?");
            var lineNumber = Console.CursorTop;
            bool ageValid;
            string ageString;
            do
            {
                ageString = Console.ReadLine();
                ageValid = InputValidator.PolicyHolder.IsAgeValid(ageString);
                if(!ageValid)
                {
                    ResetConsoleState(lineNumber);
                    lineNumber = Console.CursorTop;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("{0} is an invalid age.", string.IsNullOrEmpty(ageString) ? "Empty input" : ageString);
                    Console.ResetColor();
                    Console.WriteLine("\n\nPlease try to insert a valid age for the policy holder!");
                }
            } while (!ageValid);

            int age = int.Parse(ageString);

            Console.WriteLine("\r\nWhen was the policy holder's driving license acquired? (dd-MM-yyyy)");
            lineNumber = Console.CursorTop;

            bool licenseStartDateIsValid;
            string licenseStartDate;
            do
            {
                licenseStartDate = Console.ReadLine();
                licenseStartDateIsValid = InputValidator.PolicyHolder.IsLicenseStartDateValid(licenseStartDate);

                if (!licenseStartDateIsValid)
                {
                    ResetConsoleState(lineNumber);
                    lineNumber = Console.CursorTop;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("{0} is an Invalid start date for the driving licence.", string.IsNullOrEmpty(licenseStartDate)?"Empty input":licenseStartDate);
                    Console.ResetColor();
                    Console.WriteLine("\r\nPlease try again to insert a valid start date!\r\nThe format must be like dd-MM-yyyy and please note that date can't be in the future and also not older than 10 years!");
                }
            } while (!licenseStartDateIsValid);

            Console.WriteLine("\r\nPlease provide the 4 digits of the policy holder's postal code (e.g. 1234)");
            lineNumber = Console.CursorTop;

            bool isPostalCodeValid;
            string postalCodeString;
            do
            {
                postalCodeString = Console.ReadLine();
                isPostalCodeValid = InputValidator.PolicyHolder.IsPostalCodeValid(postalCodeString);

                if (!isPostalCodeValid)
                {
                    ResetConsoleState(lineNumber);
                    lineNumber = Console.CursorTop;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("{0} is an Invalid postal code.", string.IsNullOrEmpty(postalCodeString) ? "Empty input" : postalCodeString);
                    Console.ResetColor();
                    Console.WriteLine("\r\nPlease try again to insert a valid (4 digits only) postal code!(e.g. 1234)");
                }
            } while (!isPostalCodeValid);

            int postalCode = int.Parse(postalCodeString);

            Console.WriteLine("\r\nPlease provide the number of No-Claim years:");
            lineNumber = Console.CursorTop;

            bool isNoClaimYearsValid;
            string noClaimYearsInput;
            do
            {
                noClaimYearsInput = Console.ReadLine();
                isNoClaimYearsValid = InputValidator.PolicyHolder.IsNoClaimYearsValid(noClaimYearsInput);

                if (!isNoClaimYearsValid)
                {
                    ResetConsoleState(lineNumber);
                    lineNumber = Console.CursorTop;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("{0} is an Invalid number of no claim years.", string.IsNullOrEmpty(noClaimYearsInput) ? "Empty input" : noClaimYearsInput);
                    Console.ResetColor();
                    Console.WriteLine("\r\nPlease try again to insert the number of No-Claim years!");
                }
            } while (!isNoClaimYearsValid);

            int noClaimYears = int.Parse(noClaimYearsInput);

            return new PolicyHolder(age, licenseStartDate, postalCode, noClaimYears);
        }

        private static Vehicle GetVehicleData()
        {
            Console.WriteLine("Please provide the vehicle details...");
            Console.WriteLine("What is the vehicle's construction year?");
            var lineNumber = Console.CursorTop;

            bool isConstructYrValid;
            string constructYrString;
            do
            {
                constructYrString = Console.ReadLine();
                isConstructYrValid = InputValidator.Vehicle.IsConstructYearValid(constructYrString);

                if (!isConstructYrValid)
                {
                    ResetConsoleState(lineNumber);
                    lineNumber = Console.CursorTop;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("{0} is an Invalid construction year.", string.IsNullOrEmpty(constructYrString) ? "Empty input" : constructYrString);
                    Console.ResetColor();
                    Console.WriteLine($"\r\nPlease try again to insert an year number from 100 years ago until this year!(e.g. {DateTime.Now.Year})");
                }
            } while (!isConstructYrValid);
            int constructionYr = int.Parse(constructYrString);

            Console.WriteLine("\r\nWhat is the vehicle's value in euro's?");
            lineNumber = Console.CursorTop;

            bool isValueValid;
            string valueString;
            do
            {
                valueString = Console.ReadLine();
                isValueValid = InputValidator.Vehicle.IsValueValid(valueString);

                if (!isValueValid)
                {
                    ResetConsoleState(lineNumber);
                    lineNumber = Console.CursorTop;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("{0} is an Invalid value for the vehicle.", string.IsNullOrEmpty(valueString) ? "Empty input" : valueString);
                    Console.ResetColor();
                    Console.WriteLine("\r\nPlease try again to insert the value of the vehicle in euro's!");
                }
            } while (!isValueValid);
            int value = int.Parse(valueString);

            Console.WriteLine("What is the vehicle's power in Kw");
            lineNumber = Console.CursorTop;
            
            bool isPowerValid;
            string powerString;
            do
            {
                powerString = Console.ReadLine();
                isPowerValid = InputValidator.Vehicle.IsPowerInKWValid(powerString);

                if (!isPowerValid)
                {
                    ResetConsoleState(lineNumber);
                    lineNumber = Console.CursorTop;
                    Console.ForegroundColor = ConsoleColor.DarkRed;
                    Console.WriteLine("{0} is an Invalid value in KW for power of the vehicle.", string.IsNullOrEmpty(powerString) ? "Empty input" : powerString);
                    Console.ResetColor();
                    Console.WriteLine("\r\nPlease try again to insert the power in KW of the vehicle!");
                }
            } while (!isPowerValid);
            int powerInKw = int.Parse(powerString);

            return new Vehicle(powerInKw, value, constructionYr);
        }


        private static InsuranceCoverage GetCoverage()
        {
            Console.WriteLine("Please provide the desired coverage:");
            Console.WriteLine("1 - WA");
            Console.WriteLine("2 - WA +");
            Console.WriteLine("3 - All Risk");
            char coverageInput = Console.ReadKey(true).KeyChar;

            switch (coverageInput)
            {
                case '1':
                    return InsuranceCoverage.WA;
                case '2':
                    return InsuranceCoverage.WA_PLUS;
                case '3':
                    return InsuranceCoverage.ALL_RISK;               
                default:
                    DisplayError("Unknown vehicle type. Please Try again!");
                    return GetCoverage();
            }
        }

        private static void DisplayPremium(PolicyHolder policyholder, Vehicle vehicle, InsuranceCoverage coverage)
        {
            PremiumCalculation calculation = new(vehicle, policyholder, coverage);

            Console.WriteLine("Please provide the desired premium period:");
            Console.WriteLine("1 - Month");
            Console.WriteLine("2 - year");
            char periodInput = Console.ReadKey(true).KeyChar;
            
            PaymentPeriod p;
            string timeunit;

            switch (periodInput)
            {                
                case '2':
                    p = PaymentPeriod.YEAR;
                    timeunit = "year";
                    break;
                default:
                    p = PaymentPeriod.MONTH;
                    timeunit = "month";
                    break;
            }

            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("------------------------------------------");
            Console.WriteLine("Your premium is {0} per {1}", calculation.PremiumPaymentAmount(p), timeunit); 
            Console.WriteLine("------------------------------------------");
            Console.ResetColor();
            Console.WriteLine("\r\nPress any key to quit.");
            Console.ReadKey(true);
        }

        private static void DisplayError(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("[ERROR] {0}", message);
            Console.ResetColor();
        }

        public static void ResetConsoleState(int lineToClearFrom)
        {
            for (var line = Console.CursorTop; line >= lineToClearFrom; line--)
            {
                Console.SetCursorPosition(0, line);
                Console.Write(new string(' ', Console.WindowWidth));
            }
            Console.SetCursorPosition(0, lineToClearFrom);
        }
    }
}
