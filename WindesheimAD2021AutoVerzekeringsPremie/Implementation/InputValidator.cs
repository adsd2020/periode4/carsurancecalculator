﻿namespace WindesheimAD2021AutoVerzekeringsPremie.Implementation
{
    using System;
    using System.Globalization;

    internal class InputValidator
    {
        public static class PolicyHolder
        {
            public static bool IsAgeValid(string age)
            {
                if (!int.TryParse(age, out var result))
                    return false;

                return (result > 0 && result < 150); //Need requirement to set a better border values. Ask the client!
            }

            public static bool IsLicenseStartDateValid(string startDate)
            {
                if (!DateTime.TryParseExact(
                    startDate, "d-M-yyyy", new CultureInfo("nl-NL"), DateTimeStyles.None, out var result))
                    return false;
                
                return (result <= DateTime.Now && result>DateTime.Now.AddYears(-10));
            }

            public static bool IsPostalCodeValid(string postalCode)
            {
                if (!int.TryParse(postalCode, out var result))
                    return false;

                return (result >= 1000 && result <= 9999);
            }

            public static bool IsNoClaimYearsValid(string noClaimYears)
            {
                if (!int.TryParse(noClaimYears, out var result))
                    return false;

                return (result >= 0);
            }
        }

        public static class Vehicle
        {
            public static bool IsConstructYearValid(string constructYr)
            {
                if (!int.TryParse(constructYr, out var result))
                    return false;
                return (result > DateTime.Now.AddYears(-100).Year && result <= DateTime.Now.Year);
            }
            public static bool IsValueValid(string value)
            {
                if (!int.TryParse(value, out var result))
                    return false;
                return result > 0;
            }
            public static bool IsPowerInKWValid(string power)
            {
                if (!int.TryParse(power, out var result))
                    return false;
                return result > 0;
            }
        }
    }
}